# track-domain.go

track-domain is light golang module to track when domain is available for registration

## Overview

track-domain.go: A golang module for tracking domain.

## Build

    GOOS=linux GOARCH=amd64 go build track-domain.go

## LICENSE

Copyright 2015, shimas