/*
 * Track if domain is available for registration
 * https://www.nonamehosts.com/
 *
 * Copyright 2017, Darius Simanel
 *
 */

package main

import (
	"fmt"
	"net"
	"os"
	"strings"
	"bytes"
	"log"
	"net/smtp"
)

var myEmail = "darius.simanel@gmail.com"

func whois(domainName, server string) string {
	 conn, err := net.Dial("tcp", server+":43")

	 if err != nil {
	         fmt.Println("Error")
	 }

	 defer conn.Close()

	 conn.Write([]byte(domainName + "\r\n"))
	 buf := make([]byte, 1024)
	 result := []byte{}

	 for {
	         numBytes, err := conn.Read(buf)
	         sbuf := buf[0:numBytes]
	         result = append(result, sbuf...)
	         if err != nil {
	                 break
	         }
	 }

	 return string(result)
 }

func sendEmail(rcpt, domain string, status string) {

	c, err := smtp.Dial("localhost:25")
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()
	// Set the sender and recipient.
	c.Mail("info@nonamehosts.com")
	c.Rcpt(rcpt)
	// Send the email body.
	wc, err := c.Data()
	if err != nil {
		log.Fatal(err)
	}
	defer wc.Close()
	buf := bytes.NewBufferString("Subject: " + domain + ": whois status update\n")
    buf.WriteString(status)

	if _, err = buf.WriteTo(wc); err != nil {
		log.Fatal(err)
	}

}

func main() {
	
	var p = fmt.Println

	if len(os.Args) < 2 {
		p("Please enter domain")
	
	}else{

		domain := os.Args[1]
		result := whois(domain, "com.whois-servers.net")

		domainStatus := strings.Index(result, "Status:")

		if domainStatus == -1 {
			sendEmail(myEmail, domain, "Domain name seems to be free")
			return
		}

    	domainStatus_end := strings.Index(result[domainStatus:], "\n")
    	status := strings.Trim(strings.Replace(result[domainStatus:domainStatus + domainStatus_end], "\r", "", -1), " ")
    	status = strings.Replace(status, ":", "", -1)
    	
    	sendEmail(myEmail, domain, status)
			
	}
}